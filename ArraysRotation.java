package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        // write your code here
        int[] array = {1, 2, 3, 4, 5};
        int rotateIndex = 4;
        System.out.println(Arrays.toString(rotLeft(array, rotateIndex)));
    }

    static int[] rotLeft(int[] a, int d) {
        int arrLength = a.length;
        int[] result = new int[]{};
        if ((arrLength - d) == 0) {
            return a;
        } else {
            int[] bufferMain = new int[d];
            int[] bufferTail = new int[]{};
            System.arraycopy(a, 0, bufferMain, 0, d);
            bufferTail = Arrays.copyOfRange(a, d, arrLength);

            int aLen = bufferMain.length;
            int bLen = bufferTail.length;
            result = new int[aLen + bLen];

            System.arraycopy(bufferTail, 0, result, 0, bLen);
            System.arraycopy(bufferMain, 0, result, bLen, aLen);
        }
        return result;
    }
}
// 1 2 3 4
