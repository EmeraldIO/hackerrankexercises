package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here

        int n= 4;
        for (int i = 0; i <=n ; i++) {

            if (i > 0) {
                for (int j = i; j < n; j++) {
                    System.out.print(" ");
                }
            }
            for (int k = 0; k < i; k++) {
                System.out.print("#");
            }
            if (i>0) {
                System.out.println();
            }
        }
    }
}
