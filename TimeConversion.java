package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
    String testStr = "07:05:45PM";
        System.out.println(timeConversion(testStr));
    }

    static String timeConversion(String s) {
        String[] parts = new String[]{};
        if (s.contains("AM")) {
            String cleanStr = s.replaceAll("AM", "");
            parts = cleanStr.split(":");
            String number = parts[0];
            int result = Integer.parseInt(number);
            if (result == 12) {
                parts[0] = "00";
            }
        } else {
            String cleanStr = s.replaceAll("PM", "");
            parts = cleanStr.split(":");
            String number = parts[0];
            int result = Integer.parseInt(number);
            if (result == 12) {
                return cleanStr;
            } else {
                int buf = result + 12;
                parts[0] = String.valueOf(buf);
            }

        }
        return String.join(":", parts);

    }
}
