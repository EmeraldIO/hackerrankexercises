package com.company;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[][] test = {{1, 1, 1, 0, 0, 0, 1},
                        {0, 1, 0, 0, 0, 0, 1},
                        {1, 1, 1, 0, 0, 0, 1},
                        {0, 0, 2, 4, 4, 0, 1},
                        {0, 0, 0, 2, 0, 0, 1},
                        {0, 0, 1, 2, 4, 0, 1}};
        System.out.println(hourglassSum(test));
    }

    static int hourglassSum(int[][] arr) {
        int rows = arr.length;
        int columns = arr[0].length;
        int sum = Integer.MIN_VALUE;
        int tempSum;
        int boundaryR = (rows - 3) + 1;
        int boundaryC = (columns - 3) + 1;
        for (int i = 0; i < boundaryR; i++) {
            for (int j = 0; j < boundaryC; j++) {
                tempSum = arr[0 + i][0 + j] + arr[0 + i][1 + j] + arr[0 + i][2 + j]
                                            + arr[1 + i][1 + j] +
                          arr[2 + i][0 + j] + arr[2 + i][1 + j] + arr[2 + i][2 + j];
                if (tempSum > sum) {
                    sum = tempSum;
                }
            }
        }
        return sum;
    }
}

