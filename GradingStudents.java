package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here
        int[] test = {73, 67, 38, 33};// -> 75, 67, 40 , 33  if less 38 return same
        int[] test2 = gradingStudents(test);
        for (int i = 0; i <test2.length ; i++) {
            System.out.println(test2[i]);
        }
    }

    static int[] gradingStudents(int[] grades) {
        int[] result = new int[grades.length];
        for (int i = 0; i <grades.length ; i++) {
            double temp = (5-(Math.ceil(grades[i]%5)));
            int tempInt = (int)temp;
            if(temp<3 && grades[i]>=38){
                result[i]=grades[i]+tempInt;
            }else{
                result[i]=grades[i];
            }
        }
        return result;
    }
}

